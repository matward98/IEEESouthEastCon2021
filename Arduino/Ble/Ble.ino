#include <Arduino_LSM9DS1.h>
#include <PDM.h>
#include <voicedetect-alphawhiskey_inference.h>

#define EIDSP_QUANTIZE_FILTERBANK   0

const String WHOAMI = "Ble";
const float CORRECTION_FACTOR = .418;

/** Audio buffers, pointers and selectors */
typedef struct {
    int16_t *buffer;
    uint8_t buf_ready;
    uint32_t buf_count;
    uint32_t n_samples;
} inference_t;

static inference_t inference;
static bool record_ready = false;
static signed short sampleBuffer[2048];
static bool debug_nn = false; // Set this to true to see e.g. features generated from the raw signal
float Alpha, Whiskey, Noise;

void setup()
{
	Serial.begin(115200);
	while(!Serial)
	{

	}

	if (!IMU.begin()) //Initialize IMU sensor
  {
		Serial.println("Failed to initialize IMU!");
	}

	//Serial.println("Edge Impulse Inferencing Demo");

	// summary of inferencing settings (from model_metadata.h)
	ei_printf("Inferencing settings:\n");
	ei_printf("\tInterval: %.2f ms.\n", (float)EI_CLASSIFIER_INTERVAL_MS);
	ei_printf("\tFrame size: %d\n", EI_CLASSIFIER_DSP_INPUT_FRAME_SIZE);
	ei_printf("\tSample length: %d ms.\n", EI_CLASSIFIER_RAW_SAMPLE_COUNT / 16);
	ei_printf("\tNo. of classes: %d\n", sizeof(ei_classifier_inferencing_categories) / sizeof(ei_classifier_inferencing_categories[0]));

	if (microphone_inference_start(EI_CLASSIFIER_RAW_SAMPLE_COUNT) == false) {
			ei_printf("ERR: Failed to setup audio sampling\r\n");
			return;
	}
}

/*
*  prepareMsg(String in)
* Forms a message consistent with the message format on the RasPi.
* @in - the message portion of the message
* returns String - the message in with from and to fields appended to the front.
*/
String prepareMsg(String to, String msg) {
  String out = "`"+WHOAMI+"`"+to+"`"+msg+'`';
  return out;
}

/*
* Checks the Serial input buffer for input, If there is input available, reads
* it in, echos it back to sender for debugging, and returns parseMessage(input)
* Returns - After checking to make sure that propulsion is the intended
* recipient returns the message field of the message.
*/
String checkForInput() {
  String input = "";
  if(Serial.available()) {
    input = Serial.readString();
    String echo = "Echo: "+input;
    Serial.println(echo);
  }
  return parseMessage(input);
}

/*
* parseMessage(String input)
* Breaks the incoming message into its three fields, confirms intended
* recipient, and returns the message field.
* @input - the fully formed three field message
* returns - message field of input.
*/
String parseMessage(String input) {
  String from;
  String to;
  String msg;

  input = input.substring(input.indexOf('`')+1);
  from = input.substring(0,input.indexOf('`'));
  input = input.substring(input.indexOf('`')+1);
  to = input.substring(0,input.indexOf('`'));
  input = input.substring(input.indexOf('`')+1);
  msg = input.substring(0,input.indexOf('`'));

  if(to.equals(WHOAMI)) {
    return msg;
  }
  else {
    return "";
  }
}

void iMULoop()
{
	while(1)
	{
		float gyro_x, gyro_y, gyro_z;
	  float headings = 180.00;
	  while(1)
			{
	    String input = checkForInput();
			if(input.equals("Reset"))
			{
				break;
			}

	    int starttime = micros();

	    //Gyroscope values
	    if (IMU.gyroscopeAvailable()) {
	      IMU.readGyroscope(gyro_x, gyro_y, gyro_z);
	    }

	    delay(50);

	    int endtime = micros();
	    int operatingTime = endtime - starttime;
	    headings = headings + ((gyro_z + CORRECTION_FACTOR) * operatingTime / 1000000.00);
		String out = prepareMsg("RasSense",String(headings));
		Serial.println(out);
      	//Serial.println(String(operatingTime));
	  }
	}
}

String getVoiceInput() {
  int counter = 0;
  while (counter < 2 && counter > -2)
  {
    String output = voiceDetect();
    Serial.print("Output: ");
    Serial.println(output);
    if(output.equals("Alpha")) {
      counter++;
    }
    else if (output.equals("Whiskey")) {
      counter--;
    }
    Serial.print("counter : ");
    Serial.println(String(counter));
  }

  digitalWrite(LED_BUILTIN, HIGH);
  delay(250);
  digitalWrite(LED_BUILTIN, LOW);
  delay(250);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(250);
  digitalWrite(LED_BUILTIN, LOW);
  delay(250);

  if (counter == 2) {
    return "Alpha";
  }
  else if (counter == -2) {
    return "Whiskey";
  }
}

String voiceDetect() {
  ei_printf("Starting inferencing in 2 seconds...\n");

    delay(2000);

    digitalWrite(LED_BUILTIN, HIGH);
    ei_printf("Recording...\n");

    bool m = microphone_inference_record();
    if (!m) {
        ei_printf("ERR: Failed to record audio...\n");
        //return;
    }

    ei_printf("Recording done\n");
    digitalWrite(LED_BUILTIN, LOW);

    signal_t signal;
    signal.total_length = EI_CLASSIFIER_RAW_SAMPLE_COUNT;
    signal.get_data = &microphone_audio_signal_get_data;
    ei_impulse_result_t result = { 0 };

    EI_IMPULSE_ERROR r = run_classifier(&signal, &result, debug_nn);
    if (r != EI_IMPULSE_OK) {
        ei_printf("ERR: Failed to run classifier (%d)\n", r);
        //return;
    }

    // print the predictions
    ei_printf("Predictions ");
    ei_printf("(DSP: %d ms., Classification: %d ms., Anomaly: %d ms.)",
        result.timing.dsp, result.timing.classification, result.timing.anomaly);
    ei_printf(": \n");

    for(size_t ix = 0; ix < 1; ix++) {
      Alpha = result.classification[ix].value;
      Serial.print("Alpha: ");
      Serial.println(Alpha);
    }
    for(size_t ix = 1; ix < 2; ix++) {
      Noise = result.classification[ix].value;
      Serial.print("Noise: ");
      Serial.println(Noise);
    }
    for(size_t ix = 2; ix < 3; ix++) {
      Whiskey = result.classification[ix].value;
      Serial.print("Whiskey: ");
      Serial.println(Whiskey);
    }

    if (Alpha > 0.65) {
      return "Alpha";
    }
    else if (Whiskey > 0.65) {
      return "Whiskey";
    }
    else {
      return "Noise";
    }
}

/**
 * @brief      Printf function uses vsnprintf and output using Arduino Serial
 *
 * @param[in]  format     Variable argument list
 */
void ei_printf(const char *format, ...) {
    static char print_buf[1024] = { 0 };

    va_list args;
    va_start(args, format);
    int r = vsnprintf(print_buf, sizeof(print_buf), format, args);
    va_end(args);

    if (r > 0) {
        Serial.write(print_buf);
    }
}

/**
 * @brief      PDM buffer full callback
 *             Get data and call audio thread callback
 */
static void pdm_data_ready_inference_callback(void)
{
    int bytesAvailable = PDM.available();

    // read into the sample buffer
    int bytesRead = PDM.read((char *)&sampleBuffer[0], bytesAvailable);

    if (record_ready == true || inference.buf_ready == 1) {
        for(int i = 0; i < bytesRead>>1; i++) {
            inference.buffer[inference.buf_count++] = sampleBuffer[i];

            if(inference.buf_count >= inference.n_samples) {
                inference.buf_count = 0;
                inference.buf_ready = 1;
                break;
            }
        }
    }
}

/**
 * @brief      Init inferencing struct and setup/start PDM
 *
 * @param[in]  n_samples  The n samples
 *
 * @return     { description_of_the_return_value }
 */
static bool microphone_inference_start(uint32_t n_samples)
{
    inference.buffer = (int16_t *)malloc(n_samples * sizeof(int16_t));

    if(inference.buffer == NULL) {
        return false;
    }

    inference.buf_count  = 0;
    inference.n_samples  = n_samples;
    inference.buf_ready  = 0;

    // configure the data receive callback
    PDM.onReceive(&pdm_data_ready_inference_callback);

    // optionally set the gain, defaults to 20
    PDM.setGain(80);
    PDM.setBufferSize(4096);

    // initialize PDM with:
    // - one channel (mono mode)
    // - a 16 kHz sample rate
    if (!PDM.begin(1, EI_CLASSIFIER_FREQUENCY)) {
        ei_printf("Failed to start PDM!");
        microphone_inference_end();

        //return false;
    }
    record_ready = true;
    return true;
}

/**
 * @brief      Wait on new data
 *
 * @return     True when finished
 */
static bool microphone_inference_record(void)
{
    inference.buf_ready = 0;
    inference.buf_count = 0;

    while(inference.buf_ready == 0) {
        delay(10);
    }

    return true;
}

/**
 * Get raw audio signal data
 */
static int microphone_audio_signal_get_data(size_t offset, size_t length, float *out_ptr)
{

    arm_q15_to_float(&inference.buffer[offset], out_ptr, length);
    return 0;
}

/**
 * @brief      Stop PDM and release buffers
 */
static void microphone_inference_end(void)
{
    PDM.end();
    free(inference.buffer);
}

void loop()
{
	String input = checkForInput();
	if(input.equals("Get Voice"))
	{
		String letter = getVoiceInput();
		String out = prepareMsg("RasVoice",letter);
		Serial.println(out);

	}
	if(input.equals("IMU Mode"))
	{
		iMULoop();
	}
}

#if !defined(EI_CLASSIFIER_SENSOR) || EI_CLASSIFIER_SENSOR != EI_CLASSIFIER_SENSOR_MICROPHONE
#error "Invalid model for current sensor."
#endif
