void setup()
{
	Serial.begin(115200);
	while(!Serial)
	{
		
	}
}

void loop()
{
	checkForInput();
}

/*
* Checks the Serial input buffer for input, If there is input available, reads
* it in, echos it back to sender for debugging, and returns parseMessage(input)
* Returns - After checking to make sure that propulsion is the intended
* recipient returns the message field of the message.
*/
void checkForInput()
{
  long tik = millis();
  String input = "";
  if(Serial.available())
  {
    input = Serial.readString();
		String echo = "Echo: "+input;
		Serial.println(echo);
    long tok = millis();
    Serial.println(String(tok-tik));
  } 
 }
