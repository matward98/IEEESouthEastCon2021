#include <Arduino_LSM9DS1.h> //Include the library for 9-axis IMU

/**********************************************************************
Funcion name: resetFunc
Input parameters: NA
Return value:     NA
Purpose:          To reset the arduino mega when called
**********************************************************************/
void(*resetFunc) (void) = 0;

void setup(){
  Serial.begin(115200); //Serial monitor to display all sensor values 

  if (!IMU.begin()) //Initialize IMU sensor
  { Serial.println("Failed to initialize IMU!"); while (1);}

}

/*********************************************************************
 * Loop() behavior
 * Wait for 's' to begun IMU behavior
 * While no 'r' has been passed
 * Start timer
 * Correct heading to be 0.00 to 360.00
 * Print gyroscope readout (FOR DEBUGGING PURPOSES ONLY)
 * Read values from gryoscope (z corrsponds to yaw)
 * delay .1 second (to slow execution time)
 * end timer
 * calculate run time
 * multiply z gyroscope value by execution time (integrate) and add that to previous heading
 *
 * code is also included for correction factor to reduce gyroscope drift rate
 ********************************************************************/
void loop(){
  static double gyro_x, gyro_y, gyro_z;
  double headings = 90.00;
  static double correctionFactor = -1 * -.418;
  while((!Serial.available())||(Serial.read() != 's')){

  }

  while((!Serial.available())||(Serial.read() != 'r')){

    int starttime = micros();

    delay(100);

    if (headings > 360.00){
      headings = 0.00 + (headings - 360.00);
    }
    else if (headings < 0.00){
      headings = 360 - headings;
    }

    Serial.print("Heading: "); Serial.println(headings);
    Serial.print("Gyro_z: "); Serial.print(gyro_z);
    Serial.println("_____________________________________________________");

    //Gyroscope values
    if (IMU.gyroscopeAvailable()) {
      IMU.readGyroscope(gyro_x, gyro_y, gyro_z);
    }

    delay(100);

    int endtime = micros();
    int operatingTime = endtime - starttime;
    headings = headings + ((gyro_z + correctionFactor) * operatingTime / 1000000.00);
  }

}
