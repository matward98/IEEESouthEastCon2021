#define ME1 3 //Motor Extract 1 pin D3
#define ME2 5 //Motor Extract 2 pin D5
#define MR1 6 //Motor Rotate 1 pin D6
#define MR2 9 //Motor Rotate 2 pin D9
//PWM from 0-255
#define FSPEED 255 //full speed
#define HSPEED 128 //half speed
#define QSPEED 64 //Quarter speed
#define ESPEED 32 //eighth speed
#define SSPEED 16 //sixteenth speed

const String WHOAMI = "Arm";
int currentStatus = 0; //1 = extend, 2 = retract, 3 = rotate cw, 4 = rotate ccw
int currentSpeed = 0;

void setup() {
  Serial.begin(115200);
  while(!Serial){
  }

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(ME1, OUTPUT);
  pinMode(ME2, OUTPUT);
  pinMode(MR1, OUTPUT);
  pinMode(MR2, OUTPUT);
}

void loop() {
  String input = checkForInput();   //Check for input
  procesInput(input);               //Process the input
}

/*
*  prepareMsg(String in)
* Forms a message consistent with the message format on the RasPi.
* @in - the message portion of the message
* returns String - the message in with from and to fields appended to the front.
*/
String prepareMsg(String in) {
  String out = "`"+WHOAMI+"`RasArm`"+in+'`';
  return out;
}

/*
* Checks the Serial input buffer for input, If there is input available, reads
* it in, echos it back to sender for debugging, and returns parseMessage(input)
* Returns - After checking to make sure that propulsion is the intended
* recipient returns the message field of the message.
*/
String checkForInput() {
  String input = "";
  if(Serial.available()) {
    input = Serial.readString();
    String echo = "Echo: "+input;
    Serial.println(echo);
  }
  return parseMessage(input);
}

/*
* parseMessage(String input)
* Breaks the incoming message into its three fields, confirms intended
* recipient, and returns the message field.
* @input - the fully formed three field message
* returns - message field of input.
*/
String parseMessage(String input) {
  String from;
  String to;
  String msg;

  input = input.substring(input.indexOf('`')+1);
  from = input.substring(0,input.indexOf('`'));
  input = input.substring(input.indexOf('`')+1);
  to = input.substring(0,input.indexOf('`'));
  input = input.substring(input.indexOf('`')+1);
  msg = input.substring(0,input.indexOf('`'));

  if(to.equals(WHOAMI)) {
    return msg;
  }
  else {
    return "";
  }
}

/*
* procesInput(String input)
* given a string, determines if the string is a recognized command to the
* propulsion system. If it is, checks to see if that command is a change from
* the current state. If it is, executes the command and updates the current
* state.
* @input - the potential command
*/
void procesInput(String input)
{
	if(!input.equals(""))
	{
		String inputDir = "";
		String inputSpd = "";
		//Checks for the colon field seperator, if present seperates command into
		//direction:speed
		if(input.indexOf(':')>=0)
		{
		 inputDir = input.substring(0,input.indexOf(':'));
		 inputSpd = input.substring(input.indexOf(':')+1);
		}
		else
		{
		 inputDir = input;
		}

		int status = -1;
		int Speed = 0;
		//Checks for the Brake command
		if(inputDir.equals("Brake"))
		{
		 status = 0;
		}
		//If the command is not brake, and there is no speed, then the command is
		//invalid, else parse speed and direction.
		else if(!inputSpd.equals(""))
		{
		 if(inputDir.equals("Extend"))
		 {
			 status = 2;
		 }
		 else if(inputDir.equals("Retract"))
		 {
			 status = 1;
		 }
		 else if(inputDir.equals("Clockwise"))
		 {
			 status = 3;
		 }
		 else if(inputDir.equals("Counter"))
		 {
			 status = 4;
		 }

		 if(inputSpd.equals("Full"))
		 {
			 Speed = FSPEED;
		 }
		 else if(inputSpd.equals("Half"))
		 {
			 Speed = HSPEED;
		 }
		 else if(inputSpd.equals("Quarter"))
		 {
			 Speed = QSPEED;
		 }
		 else if(inputSpd.equals("Eighth"))
		 {
			 Speed = ESPEED;
		 }
		 else if(inputSpd.equals("Sixteenth"))
		 {
			 Speed = SSPEED;
		 }
	 }

	 //If input status is different from current status, update current status and change behavior.
	 if((status != currentStatus && status != -1)||(Speed != currentSpeed))
	 {
		 if(status == 0) //Brake
		 {
			 analogWrite(ME1,0);
			 analogWrite(ME2,0);
			 analogWrite(MR1,0);
			 analogWrite(MR2,0);
			 digitalWrite(LED_BUILTIN,LOW);
			 String echo = prepareMsg("Stopping");
			 Serial.println(echo);
			 currentStatus = 0;
			 currentSpeed = 0;
		 }
		 else if(status == 1) // Forward
		 {
			 analogWrite(ME1,Speed);
			 analogWrite(ME2,0);
			 analogWrite(MR1,0);
			 analogWrite(MR2,0);
			 digitalWrite(LED_BUILTIN,HIGH);
			 String echo = prepareMsg("Retract");
			 Serial.println(echo);
			 currentStatus = 1;
			 currentSpeed = Speed;
		 }
		 else if(status == 2) //Reverse
		 {
			 analogWrite(ME1,0);
			 analogWrite(ME2,Speed);
			 analogWrite(MR1,0);
			 analogWrite(MR2,0);
			 digitalWrite(LED_BUILTIN,HIGH);
			 String echo = prepareMsg("Extend");
			 Serial.println(echo);
			 currentStatus = 2;
			 currentSpeed = Speed;
		 }
		 else if(status == 3) //Port Turn
		 {
			 analogWrite(ME1,0);
			 analogWrite(ME2,0);
			 analogWrite(MR1,0);
			 analogWrite(MR2,Speed);
			 digitalWrite(LED_BUILTIN,HIGH);
			 String echo = prepareMsg("Clockwise");
			 Serial.println(echo);
			 currentStatus = 3;
			 currentSpeed = Speed;
		 }
		 else if(status == 4) //Starboard Turn
		 {
			 analogWrite(ME1,0);
			 analogWrite(ME2,0);
			 analogWrite(MR1,Speed);
			 analogWrite(MR2,0);
			 analogWrite(LED_BUILTIN,HIGH);
			 String echo = prepareMsg("Counter");
			 Serial.println(echo);
			 currentStatus = 4;
			 currentSpeed = Speed;
			}
		}
	}
}
