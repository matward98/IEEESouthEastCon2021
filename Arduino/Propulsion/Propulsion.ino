#define MS1 3 //Motor Port1 pin D2
#define MS2 5 //Motor P2 pin D3
#define MP1 6 //Motor Starboard1 pin D4
#define MP2 9 //Motor S2 pin D5
//PWM from 0-255
#define FSPEED 255 //Full speed
#define HSPEED 128 //Half speed
#define QSPEED 64 //Quarter speed
#define ESPEED 32 //Eighth speed
#define SSPEED 24 //Sixteenth speed

const String WHOAMI = "NanProp";
int currentStatus = 0; // 0 = Brake, 1 = Forward, 2 = Reverse, 3 = Port, 4 = Starboard
int currentSpeed = 0;

/*
* setup()
* runs once at power on, used to setup the Arduino
*/
void setup()
{
  //Start Serial Port
  Serial.begin(115200);
	//Wait for serial port to open
	while(!Serial)
	{
	}

	//Define pinmodes
  pinMode(LED_BUILTIN,OUTPUT);
	pinMode(MP1,OUTPUT);
	pinMode(MP2,OUTPUT);
	pinMode(MS1,OUTPUT);
	pinMode(MS2,OUTPUT);
}

/*
* loop()
* main control loop, repeats continuously after setup() until power down.
*/
void loop()
{
  //Check for input
  byte input = checkForInput();
  //Process that input
  if(input != 0xff)
  {
	  procesInput(input);
  }
}

/*
*	prepareMsg(String in)
* Forms a message consistent with the message format on the RasPi.
*	@in - the message portion of the message
* returns String - the message in with from and to fields appended to the front.
*/
String prepareMsg(String in)
{
	String out = "`"+WHOAMI+"`RasProp`"+in+'`';
	return out;
}

/*
* Checks the Serial input buffer for input, If there is input available, reads
* it in, echos it back to sender for debugging, and returns parseMessage(input)
* Returns - After checking to make sure that propulsion is the intended
* recipient returns the message field of the message.
*/
byte checkForInput()
{
  byte output = 0xff;
  if(Serial.available())
  {
	  output = Serial.read();
  }
  return output;
}


/*
* procesInput(String input)
* given a string, determines if the string is a recognized command to the
* propulsion system. If it is, checks to see if that command is a change from
* the current state. If it is, executes the command and updates the current
* state.
* @input - the potential command
*/
void procesInput(byte input)
{
	byte inputDir = input & 0x30;
	byte inputSpd = input & 0x07;
		//Checks for the colon field seperator, if present seperates command into
		//direction:speed

	int status = -1;
	int speed = 0;
		//Checks for the Brake command
	if(input == 0x00)
	{
	  status = 0;
	}
		//If the command is not brake, and there is no speed, then the command is
		//invalid, else parse speed and direction.
	else if(inputSpd!=0x00)
	{
	  if(inputDir==0x00)
	  {
	    status = 1;
	  }
	  else if(inputDir==0x30)
	  {
	    status = 2;
	  }
	  else if(inputDir==0x20)
	  {
	    status = 3;
	  }
	  else if(inputDir==0x10)
	  {
	    status = 4;
	  }

	  if(inputSpd==0x05)
	  {
	    speed = FSPEED;
	  }
	  else if(inputSpd==0x04)
	  {
	    speed = HSPEED;
	  }
	  else if(inputSpd==0x03)
	  {
	    speed = QSPEED;
	  }
	  else if(inputSpd==0x02)
	  {
	    speed = ESPEED;
	  }
	  else if(inputSpd==0x01)
	  {
	    speed = SSPEED;
	  }
	}

	//If input status is different from current status, update current status and change behavior.
	if((status != currentStatus && status != -1)||(speed != currentSpeed))
	{
	  if(status == 0) //Brake
	  {
	    analogWrite(MP1,0);
	    analogWrite(MP2,0);
	    analogWrite(MS1,0);
	    analogWrite(MS2,0);
	    digitalWrite(LED_BUILTIN,LOW);
	    String echo = prepareMsg("Stopping");
	    Serial.println(echo);
	    currentStatus = 0;
	    currentSpeed = 0;
	  }
	  else if(status == 1) //Reverse
	  {
	    analogWrite(MP1,speed/4);
	    analogWrite(MP2,0);
	    analogWrite(MS1,0);
	    analogWrite(MS2,speed*93/100);
		analogWrite(MP1,speed);
	    analogWrite(MP2,0);
	    digitalWrite(LED_BUILTIN,HIGH);
	    String echo = prepareMsg("Reverse");
	    Serial.println(echo);
	    currentStatus = 1;
	    currentSpeed = speed;
	  }
	  else if(status == 2) //Forward
	  {
	    analogWrite(MP1,0);
	    analogWrite(MP2,speed/4);
	    analogWrite(MS1,speed*93/100);
	    analogWrite(MS2,0);
		analogWrite(MP1,0);
	    analogWrite(MP2,speed);
	    digitalWrite(LED_BUILTIN,HIGH);
	    String echo = prepareMsg("Forward");
	    Serial.println(echo);
	    currentStatus = 2;
	    currentSpeed = speed;
	  }
	  else if(status == 3) //Port Turn
	  {
	    analogWrite(MP1,0);
	    analogWrite(MP2,speed);
	    analogWrite(MS1,0);
	    analogWrite(MS2,speed);
	    digitalWrite(LED_BUILTIN,HIGH);
	    String echo = prepareMsg("Left");
	    Serial.println(echo);
	    currentStatus = 3;
	    currentSpeed = speed;
	  }
	  else if(status == 4) //Starboard Turn
	  {
	    analogWrite(MP1,speed);
	    analogWrite(MP2,0);
	    analogWrite(MS1,speed);
	    analogWrite(MS2,0);
	    analogWrite(LED_BUILTIN,HIGH);
	    String echo = prepareMsg("Right");
	    Serial.println(echo);
	    currentStatus = 4;
	    currentSpeed = speed;
	  }
	}
}
