 #include <Wire.h>
#include "Adafruit_VL6180X.h"
#include "Wire.h"
extern "C" {
#include "utility/twi.h"  // from Wire library, so we can do bus scanning
}

Adafruit_VL6180X vl1 = Adafruit_VL6180X();
Adafruit_VL6180X vl2 = Adafruit_VL6180X();
Adafruit_VL6180X vl3 = Adafruit_VL6180X();
Adafruit_VL6180X vl4 = Adafruit_VL6180X();
Adafruit_VL6180X vl5 = Adafruit_VL6180X();
Adafruit_VL6180X vl6 = Adafruit_VL6180X();

const String WHOAMI = "WallNano";

uint8_t sensor1Read = 255;
uint8_t sensor2Read = 255;
uint8_t sensor3Read = 255;
uint8_t sensor4Read = 255;
uint8_t sensor5Read = 255;
uint8_t sensor6Read = 255;

#define TCAADDR 0x70

void tcaselect(uint8_t i) {
  if (i > 7) return;

  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();
}

void setup() {
  Wire.begin();
  Serial.begin(115200);

  // wait for serial port to open on native usb devices
  while (!Serial) {
    delay(1);
  }
    for (uint8_t t=0; t<6; t++) {
      tcaselect(t);
    //Serial.print("TCA Port #"); Serial.println(t);
      for (uint8_t addr = 0; addr<=127; addr++) {
        if (addr == TCAADDR)
		{
			continue;
		}
        uint8_t data;
////        if (! twi_writeTo(addr, &data, 0, 1, 1)) {
////           Serial.print("Found I2C 0x");
////           Serial.println(addr,HEX);
////        }
      }
        if (t==0){
////            Serial.println("1: Adafruit VL6180x test!");
            if (! vl1.begin()) {
              Serial.println("1: Failed to find sensor");
              while (1)
			  {

			  }
            }
////            Serial.println("1: Sensor found!");
        }
        else if (t==1){
////             Serial.println("2: Adafruit VL6180x test!");
           if (! vl2.begin()) {
            Serial.println("2: Failed to find sensor");
              while (1)
			  {

			  }
            }
////            Serial.println("2: Sensor found!");
        }
        else if (t==2){
////            Serial.println("3: Adafruit VL6180x test!");
            if (! vl3.begin()) {
              Serial.println("3: Failed to find sensor");
              while (1)
			  {

			  }
            }
////            Serial.println("3: Sensor found!");
        }
        else if (t==3){
////            Serial.println("4: Adafruit VL6180x test!");
            if (! vl4.begin()) {
              Serial.println("4: Failed to find sensor");
              while (1)
			  {

			  }
            }
////            Serial.println("4: Sensor found!");
        }
		else if (t==4){
////            Serial.println("4: Adafruit VL6180x test!");
            if (! vl5.begin()) {
              Serial.println("5: Failed to find sensor");
              while (1)
			  {

			  }
            }
////            Serial.println("4: Sensor found!");
        }
		else if (t==5){
////            Serial.println("4: Adafruit VL6180x test!");
            if (! vl6.begin()) {
              Serial.println("6: Failed to find sensor");
              while (1)
			  {

			  }
            }
////            Serial.println("4: Sensor found!");
        }

    }
}

void loop()
{
  //long tik = micros();
  readSensors();
  prepareMsg();
  //long tok = micros();
  //Serial.println(String(tok-tik));
  delay(65);
}

void readSensors()
{
  tcaselect(0);
  uint8_t range1 = vl1.readRange();
  uint8_t status1 = vl1.readRangeStatus();

  if (status1 == VL6180X_ERROR_NONE)
  {
    //Serial.print("Range: "); Serial.println(range);
    sensor1Read = range1;
  }
	else
	{
		sensor1Read = 255;
	}

  tcaselect(1);
  uint8_t range2 = vl2.readRange();
  uint8_t status2 = vl2.readRangeStatus();

  if (status2 == VL6180X_ERROR_NONE)
  {
    //Serial.print("Range: "); Serial.println(range);
    sensor2Read = range2;
  }
	else
	{
		sensor2Read = 255;
	}

  tcaselect(2);
  uint8_t range3 = vl3.readRange();
  uint8_t status3 = vl3.readRangeStatus();

  if (status3 == VL6180X_ERROR_NONE)
  {
    //Serial.print("Range: "); Serial.println(range);
    sensor3Read = range3;
  }
	else
	{
		sensor3Read = 255;
	}

  tcaselect(3);
  uint8_t range4 = vl4.readRange();
  uint8_t status4 = vl4.readRangeStatus();

  if (status4 == VL6180X_ERROR_NONE)
  {
    //Serial.print("Range: "); Serial.println(range);
    sensor4Read = range4;
  }
	else
	{
		sensor4Read = 255;
	}

	tcaselect(4);
	uint8_t range5 = vl5.readRange();
	uint8_t status5 = vl5.readRangeStatus();

	if (status5 == VL6180X_ERROR_NONE)
	{
	  //Serial.print("Range: "); Serial.println(range);
	  sensor5Read = range5;
	}
	else
	{
		sensor5Read = 255;
	}

	tcaselect(5);
	uint8_t range6 = vl6.readRange();
	uint8_t status6 = vl6.readRangeStatus();

	if (status6 == VL6180X_ERROR_NONE)
	{
	  //Serial.print("Range: "); Serial.println(range);
	  sensor6Read = range6;
	}
	else
	{
		sensor6Read = 255;
	}
}

String prepareMsg()
{
	  Serial.flush();
    Serial.write(sensor1Read);
//    Serial.flush();
    Serial.write(sensor2Read);
//    Serial.flush();
    Serial.write(sensor3Read);
//    Serial.flush();
    Serial.write(sensor4Read);
	Serial.write(sensor5Read);
	Serial.write(sensor6Read);

  /*Serial.print(String(sensor1Read));
  Serial.print(":");  
  Serial.print(String(sensor2Read));
  Serial.print(":");
  Serial.print(String(sensor3Read));
  Serial.print(":");
  Serial.print(String(sensor4Read));
  Serial.print(":");
  Serial.print(String(sensor5Read));
  Serial.print(":");
  Serial.println(String(sensor6Read));*/
}
