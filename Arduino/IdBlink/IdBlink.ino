void setup()
{
  Serial.begin(115200);
  while(!Serial)
  {

  }
  pinMode(LED_BUILTIN,OUTPUT);
}

int isOn = 0;
int isBlinking = 0;
const String WHOAMI = "Nano";

void loop()
{
  String in = checkForInput();
  if(in.equals("Blink"))
  {
    isBlinking = 1;
    isOn = 1;
  }
  else if(in.equals("Stop"))
  {
    isBlinking = 0;
    isOn = 0;
  }
  else if(isBlinking)
  {
    isOn = !isOn;
  }

  if(isOn)
  {
    digitalWrite(LED_BUILTIN,HIGH);
  }
  else
  {
    digitalWrite(LED_BUILTIN,LOW);
  }

  delay(500);
}

String checkForInput()
{
  String input = "";
  if(Serial.available())
  {
    input = Serial.readString();
		String echo = "Echo: "+input;
		Serial.println(echo);
  }
  return parseMessage(input);
}

/*
* parseMessage(String input)
* Breaks the incoming message into its three fields, confirms intended
* recipient, and returns the message field.
* @input - the fully formed three field message
* returns - message field of input.
*/
String parseMessage(String input)
{
  String from;
  String to;
  String msg;

  input = input.substring(input.indexOf('`')+1);
  from = input.substring(0,input.indexOf('`'));
  input = input.substring(input.indexOf('`')+1);
  to = input.substring(0,input.indexOf('`'));
  input = input.substring(input.indexOf('`')+1);
  msg = input.substring(0,input.indexOf('`'));

  if(to.equals(WHOAMI))
  {
    return msg;
  }
  else
  {
    return "";
  }
}
