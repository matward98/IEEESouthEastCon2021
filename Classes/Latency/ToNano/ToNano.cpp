#include <iostream>
#include <unistd.h>
#include <string>
#include "Message.h"
#include "Serial.h"

using namespace std;

Serial* in = new Serial("/dev/ttyUSB0");
Message* out = new Message("Term","NanProp");
int main()
{
	while(1)
	{
			string input;
			cin>>input;
			if(!input.compare("Write"))
			{
				in->writeStream(out->writeMessage("Forward:Half"));
			}
			else if(!input.compare("q"))
			{
				break;
			}
	}
	in->closeStream();
	delete(out);
	return 0;
}
