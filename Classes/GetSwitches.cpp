#include <string>
#include <iostream>
#include <wiringPi.h>
#include <unistd.h>
#include "Message.h"

#define IS_A 1
#define G1 6
#define G2 5
#define G3 4
#define G4 3
#define G5 2
#define HIGH_FIRST 7

const string WHOAMI = "RasLoc";

using namespace std;

Message* switchToNav = new Message(WHOAMI,"RasNav");
char* switchNavMem = setUpMem<char>(120);

int main()
{
	cout<<"Enter GetSwitches Main"<<endl;
	offerHandShake(WHOAMI, "RasNav", switchNavMem);

	//Initialize Pins
	wiringPiSetup();
	//cout<<"Got Here"<<endl;

	pinMode(IS_A,INPUT);
	pinMode(G1,INPUT);
	pinMode(G2,INPUT);
	pinMode(G3,INPUT);
	pinMode(G4,INPUT);
	pinMode(G5,INPUT);
	pinMode(HIGH_FIRST,INPUT);

	pullUpDnControl(IS_A,PUD_DOWN);
	pullUpDnControl(G1,PUD_DOWN);
	pullUpDnControl(G2,PUD_DOWN);
	pullUpDnControl(G3,PUD_DOWN);
	pullUpDnControl(G4,PUD_DOWN);
	pullUpDnControl(G5,PUD_DOWN);
	pullUpDnControl(HIGH_FIRST,PUD_DOWN);

	//Read the Pins
	int switches[7] = {0,0,0,0,0,0,0};
	switches[0] = digitalRead(IS_A);
	switches[1] = digitalRead(G1);
	switches[2] = digitalRead(G2);
	switches[3] = digitalRead(G3);
	switches[4] = digitalRead(G4);
	switches[5] = digitalRead(G5);
	switches[6] = digitalRead(HIGH_FIRST);

	string out = "";
	int first = 0;

	for(int i = 0; i<6; i++)
	{
		if(i==0)
		{
			if(!switches[0])
			{
				out = "Alpha";
			}
			else
			{
				out = "Whiskey";
			}
			out += ":";
		}
		else
		{
			if(switches[6])
			{
				int j = 6-i;
				if(switches[j] && !first)
				{
					out += "G"+to_string(j)+":";
					first = 1;
				}
				else if(switches[j])
				{
					out += "G"+to_string(j);
					break;
				}
			}
			else
			{
				if(switches[i] && !first)
				{
					out += "G"+to_string(i)+":";
					first = 1;
				}
				else if(switches[i])
				{
					out += "G"+to_string(i);
					break;
				}
			}
		}
	}
	stringToChar(switchToNav->writeMessage(out),switchNavMem);
	cout<<out<<endl;
	usleep(2000000);
	cleanMem(switchNavMem,120);

	cout<<"Switches Terminating"<<endl;
	return 0;
}
