#include <iostream>
#include <unistd.h>

using namespace std;

int main()
{
  pid_t parent = getpid();
  int forks = 0;

  pid_t fork1 = fork();
  forks++;
  if(fork1 == 0)
  {
    execl("./Count.exe",NULL);
  }

  pid_t fork2 = fork();
  forks++;
  if(fork2 == 0)
  {
    execl("./InvCount.exe",NULL);
  }



  cout<<"Parent: "<<parent<<endl;
  cout<<"Child: "<<fork1<<endl;
  return 0;
}
