#include <iostream>
#include "Serial.h"

using namespace std;

Serial* out = new Serial("/dev/ttyUSB0");

int main()
{
		while(1)
		{
			string in = "";
			cin>>in;
			if(!in.compare("quit"))
			{
				break;
			}
			else
			{
				int inInt = stoi(in,0,16);
				out->writeByte(char(inInt));
			}
		}
		out->closeStream();
}
