#ifndef SERIAL
#define SERIAL
#include <string>
#include <termios.h>

using namespace std;

class Serial
{
	string portName;
	int fileStream;
public:
	Serial(string); //Opens a file handler for Serial IO

	void writeStream(string);//Writes a string over serial
	void writeByte(char);
	string readString();//Reads input strings from Serial
	int readInt();//Reads an Integer Value From the Serial
	double readDouble();//Reads double value from Serial
	void getWallSensors(int*, int*, int*, int*);//Reads 4 Ints from serial
	void closeStream();//Closes the Serial stream
};

#endif
