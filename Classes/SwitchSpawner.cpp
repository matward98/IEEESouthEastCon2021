#include <unistd.h>
#include <wiringPi.h>
#include <sys/wait.h>
#include <iostream>

#define START 0

using namespace std;
int main()
{
	wiringPiSetup();
	pinMode(START,INPUT);
	pullUpDnControl(START,PUD_DOWN);
	while(!digitalRead(START))
	{

	}
	try
	{
		int execErr = 0;
		pid_t switches = fork();
		if(switches == 0)
		{
			cout<<"Spawning Switches"<<endl;
			execErr = execl("./GetSwitches.exe",NULL);
		}
		if(switches<0 || execErr!=0)
		{
			throw 4;
		}
		pid_t navigation = fork();
		if(navigation == 0)
		{
			cout<<"Spawning Navigation"<<endl;
			execErr = execl("./Navigation.exe",NULL);
		}
		if(navigation<0 || execErr!=0)
		{
			throw 2;
		}
		int status;
		waitpid(switches,&status,0);
		pid_t propulsion = fork();
		if(propulsion == 0)
		{
			cout<<"Spawning Propulsion"<<endl;
			execErr = execl("./Propulsion.exe",NULL);
		}
		if(propulsion<0 || execErr!=0)
		{
			throw 1;
		}
		pid_t sensors = fork();
		if(sensors == 0)
		{
			cout<<"Spawning Sensors"<<endl;
			execErr = execl("./Sensors.exe",NULL);
		}
		if(sensors<0 || execErr!=0)
		{
			throw 3;
		}
		waitpid(propulsion,&status,0);
		waitpid(navigation,&status,0);
		waitpid(sensors,&status,0);
		cout<<"ProcessSpawner Terminating"<<endl;
		system("sudo journalctl -u ProcessSpawner.service >> Debug.txt");
	}
	catch(int e)
	{
		switch(e)
		{
			case 1: cerr<<"Propulsion failed to fork."<<endl;
			break;
			case 2: cerr<<"Navigation failed to fork."<<endl;
			break;
			case 3: cerr<<"Sensors failed to fork."<<endl;
			break;
			case 4: cerr<<"Switches failed to fork."<<endl;
			break;
			default:cerr<<"An unknown error occourred"<<endl;
			break;
		}
		return -1;
	}
}
