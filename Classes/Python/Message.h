#ifndef MESSAGE
#define MESSAGE

#include <string>
#include <sys/shm.h>
#include <sys/ipc.h>

using namespace std;

class Message
{
	string from;
	string to;
	string message;

public:
	Message(string,string,string=""); //Outgoing Message Constructor
	Message(string); //Incoming Message Constructor
	string transcribe(); //Returns a formatted Outgoing message
	string writeMessage(string); //Changes a message, leaves from and to alone
	string getFrom(); //Returns who a message is from
	string getTo(); //Returns who a message is to
	string getMsg(); //Returns the message
};

void stringToChar(string,char*);
string charToString(char*);
void offerHandShake(string, string, char*);
void returnHandShake(string, string, char*);
void cleanMem(void*,int);

/*
* setUpMem(int keyNum)
* @param key, a unique memory identifier, equal in both processes to coordinate
* locations.
* @returns, Char* a pointer to the string stored in shared memory.
*/
template <typename type>
type* setUpMem(int keyNum)
{
	key_t key = ftok("shm.shm",keyNum);
	int shmid = shmget(key,64,0640|IPC_CREAT);
	type* out = (type*) shmat(shmid,(void*)0,0);
	return out;
}

#endif
