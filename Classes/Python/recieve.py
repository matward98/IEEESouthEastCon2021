#! /bin/python3
import sysv_ipc
key  = sysv_ipc.ftok("shm.shm",100,1)
memory = sysv_ipc.SharedMemory(key=key,flags=sysv_ipc.IPC_CREAT,size = 64)
while 1:
	string = memory.read()
	print(string)
