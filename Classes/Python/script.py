#! /usr/bin/python3
import sysv_ipc
key  = sysv_ipc.ftok("shm.shm",100,1)
memory = sysv_ipc.SharedMemory(key=key,flags=sysv_ipc.IPC_CREAT,size = 64)#,666,64) 
string = "str"
memory.write(string)
i = 0
while i<10000000:
	i = i+1
string = "End\0"
memory.write(string)
memory.remove()
