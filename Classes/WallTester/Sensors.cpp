#include <string>
#include <iostream>
#include <unistd.h>
#include "Serial.h"
#include "Message.h"

using namespace std;

const string WHOAMI = "RasSense";

Serial* sensorNano = new Serial("/dev/cu.usbserial-AQ01698J");

Message* toWall = new Message(WHOAMI,"WallNano");
Message* out = new Message(WHOAMI,"Terminal");

int main()
{
	while(1)
	{
		usleep(67000);
		int wallSense0 = -1;
		int wallSense1 = -1;
		int wallSense2 = -1;
		int wallSense3 = -1;
		int wallSense4 = -1;
		int wallSense5 = -1;
		sensorNano->getWallSensors(&wallSense0, &wallSense1, &wallSense2, &wallSense3, &wallSense4, &wallSense5);

		if(wallSense0 != 0)
		{
			string outMsg = to_string(wallSense0)+":"+to_string(wallSense1)+":"+to_string(wallSense2)+":"+to_string(wallSense3)+":"+to_string(wallSense4)+":"+to_string(wallSense5);
			cout<<out->writeMessage(outMsg)<<endl;
		}
	}

	delete(sensorNano);
	delete(toWall);
	cout<<"Sensors Terminating"<<endl;
	return 0;
}
