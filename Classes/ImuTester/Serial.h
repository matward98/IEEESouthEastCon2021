#ifndef SERIAL
#define SERIAL
#include <string>
#include <termios.h>

using namespace std;

class Serial
{
	string portName;
	int fileStream;
public:
	Serial(string); //Opens a file handler for Serial IO

	void writeStream(string);//Writes a string over serial
	string readString();//Reads input from Serial
	void closeStream();//Closes the Serial stream
};

#endif
