#include <string>
#include <iostream>
#include <unistd.h>
#include "Serial.h"
#include "Message.h"

using namespace std;

const string WHOAMI = "RasSense";

Serial* ble = new Serial("/dev/ttyACM0");

Message* toBle = new Message(WHOAMI, "Ble");

int main()
{
	ble->writeStream(toBle->writeMessage("IMU Mode"));
	while(1)
	{
		usleep(210000);
		Message bleMsg = Message(ble->readString());
		string out;
		if(!bleMsg.getTo().compare(WHOAMI))
		{
			out = bleMsg.transcribe();
		}
		cout<<out<<endl;
	}


	delete(ble);
	delete(toBle);
	cout<<"Sensors Terminating"<<endl;
	return 0;
}
