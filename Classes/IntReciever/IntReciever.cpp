#include <iostream>
#include <string>
#include <unistd.h>
#include "Serial.h"

using namespace std;

Serial* NanoStream = new Serial("/dev/ttyUSB0");

int main()
{
	while(1)
	{
		int out1 = -1;
		int out2 = -1;
		int out3 = -1;
		int out4 = -1;
		NanoStream->getWallSensors(&out1,&out2,&out3,&out4);
		if(out1>0)
		{
			cout<<out1<<":"<<out2<<":"<<out3<<":"<<out4<<endl;
		}
		usleep(67000);
	}
	return 0;
}
