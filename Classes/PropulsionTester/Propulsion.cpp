#include <iostream>
#include <iomanip>
#include <string>
#include <thread>
#include <cstdint>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include "Serial.h"
#include "Message.h"

const string WHOAMI="RasProp";
//Propulsion Address Mac: /dev/cu.usbmodem14101
//Propulsion Address RasPi: /dev/ttyUSB0
Serial* ardNan0 = new Serial("/dev/ttyUSB0");


void propulsion(string);
void cameraFunc();

using namespace std;

int main()
{
	usleep(200000);
	/*Read the initial command, probably gibberish*/
	propulsion("Brake");
	/*Poll for new commands*/
	while(1)
	{	
		string currentCommand = "";
		cin>>currentCommand;
		Message* navMsg = new Message(currentCommand);
		if(navMsg->getTo()==WHOAMI)
		{
			if(!navMsg->getMsg().compare("End"))
			{
				break;
			}
			else
			{
				propulsion(navMsg->getMsg());
			}
		}
		delete(navMsg);
	}
	ardNan0->closeStream();
	return 0;
}

void propulsion(string order)
{
	char output = 0x00;
	if(!order.compare("Brake"))
	{
		cout<<"Enter Brake"<<endl;
		ardNan0->writeByte(output);
	}
	else if(order.find(":")<255)
	{
		cout<<"Found Colon"<<endl;
		size_t at = 0;
		string direction = order.substr(0,at = order.find(":"));
		at++;
		string speed = order.substr(at);

		if(!direction.compare("Forward"))
		{
			output &= ~(0x30);
			cout<<hex<<uppercase<<"Forward Set: "<<int(output)<<endl;
		}
		else if(!direction.compare("Reverse"))
		{
			output |= 0x30;
		}
		else if(!direction.compare("Port"))
		{
			output &= ~(0x10);
			output |= 0x20;
		}
		else if(!direction.compare("Starboard"))
		{
			output &= ~(0x20);
			output |= 0x10;
		}

		if(!speed.compare("Full"))
		{
			output &= ~(0x02);
			output |= 0x05;
		}
		else if(!speed.compare("Half"))
		{
			output &= ~(0x03);
			output |= 0x04;
			cout<<hex<<uppercase<<"Half Set: "<<int(output)<<endl;
		}
		else if(!speed.compare("Quarter"))
		{
			output &= ~(0x04);
			output |= 0x03;
		}
		else if(!speed.compare("Eighth"))
		{
			output &= ~(0x05);
			output |= 0x02;
		}
		else if(!speed.compare("Sixteenth"))
		{
			output &= ~(0x06);
			output |= 0x01;
		}
	}
	cout<<hex<<"Byte to Prop: "<<int(output)<<endl;
	ardNan0->writeByte(output);
	usleep(70000);
	string nanMsg = ardNan0->readString();
	cout<<hex<<"Nano Return: "<<nanMsg<<endl;
}
