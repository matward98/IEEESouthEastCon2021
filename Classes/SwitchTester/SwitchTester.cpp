#include<wiringPi.h>
#include<unistd.h>
#include<iostream>

using namespace std;

int main()
{
	wiringPiSetup();
	pinMode(0,INPUT);
	pullUpDnControl(0,PUD_DOWN);
	while(1)
	{
		if(digitalRead(0))
		{
			cout<<"Pin0: High"<<endl;
		}
		else
		{
			cout<<"Pin0: Low"<<endl;
		}
		usleep(1000000);
	}
	return 0;
}
