#include <string>
#include <chrono>
#include <iostream>
#include <ctime>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/time.h>
#include "Message.h"

using namespace std;
using namespace chrono;
using time_stamp = time_point<system_clock,microseconds>;

int main()
{
	time_stamp* shared12 = setUpMem<time_stamp>(100);
	*shared12 = 0;
	char* hand12 = setUpMem<char>(101);
	time_stamp* shared23 = setUpMem<time_stamp>(102);
	*shared23 = 0;
	char* hand23 = setUpMem<char>(103);

	returnHandShake("Process1","Process2",hand12);
	offerHandShake("Process2","Process3",hand23);
	cout<<"Handshaking Complete"<<endl;
	int sentinal = 1;

	while(sentinal)
	{
		cout<<shared12<<":"<<shared23<<endl;
		if(0!=shared12)
		{
			*shared23 = *shared12;
			sentinal = 0;
		}
		usleep(134000);
	}
	cleanMem(shared12,100);
	cleanMem(hand12,101);
	cleanMem(shared23,102);
	cleanMem(hand23,103);
}
