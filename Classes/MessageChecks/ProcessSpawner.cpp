#include <sys/wait.h>
#include <unistd.h>

using namespace std;

int main()
{
	pid_t process1 = fork();
	if(process1 == 0)
	{
		execl("./Process1.exe",NULL);
	}
	pid_t process2 = fork();
	if(process2 == 0)
	{
		execl("./Process2.exe",NULL);
	}
	pid_t process3 = fork();
	if(process3 == 0)
	{
		execl("./Process3.exe",NULL);
	}
	int status;
	waitpid(process1,&status,0);
	waitpid(process2,&status,0);
	waitpid(process3,&status,0);
}
