#include <string>
#include <chrono>
#include <iostream>
#include <ctime>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/time.h>
#include "Message.h"

using namespace std;
using namespace chrono;
using time_stamp = time_point<system_clock,microseconds>;
int main()
{
	time_stamp* shared23 = setUpMem<time_stamp>(102);
	*shared23 = 0;
	char* hand23 = setUpMem<char>(103);
	returnHandShake("Process2","Process3",hand23);

	int sentinal = 1;

	while(sentinal)
	{
		if(0!=shared23)
		{
			time_stamp later = time_point_cast<microsecond>(system_clock::now());
			cout<<"Later: "<<duration_cast<microseconds>(later.timeSinceEpoch())<<endl;
			cout<<"Time: "<<duration_cast<microseconds>(later - *shared23)<<endl;
			sentinal = 0;
		}
		usleep(1000);
	}
	cleanMem(shared23,102);
	cleanMem(hand23,103);
}
