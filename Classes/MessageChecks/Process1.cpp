#include <string>
#include <chrono>
#include <iostream>
#include <ctime>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/time.h>
#include "Message.h"

using namespace std;
using namespace chrono;
using time_stamp = time_point<system_clock,microseconds>;
int main()
{
	time_stamp* shared = setUpMem<time_stamp>(100);
	char* hand = setUpMem<char>(101);
	offerHandShake("Process1","Process2",hand);
	int sentinal = 1;
	while(sentinal)
	{
		cout<<"Type \"Now\" to begin\t";
		string in;
		cin>>in;
		if(!in.compare("Now"))
		{
			time_stamp now = time_point_cast<microseconds>(system_clock::now());
			*shared = now;
			cout<<"Now: "<<duration_cast<microseconds>(now.time_since_epoch())<<endl;
			sentinal = 0;
		}
	}
	cleanMem(shared,100);
}
