#include "Message.h"
#include <iostream>
#include <sys/shm.h>
#include <sys/ipc.h>
/*
* Message(string msgFrom, string msgTo, string msg)
* Outgoing Message Constructor
* @msgFrom, Who is sending the message. Generally set to WHOAMI
* @msgTo, Who is the intended recipient of the message
* @msg, Optional to construct with the message field
* returns pointer to new Message
*/
Message::Message(string msgFrom, string msgTo, string msg)
{
	from = msgFrom;
	to = msgTo;
	message = msg;
}

/*
* Message(string in)
* Incoming Message Constructor,
* Given a string that contains the message it will parse for all three fields
* into their respective memory locations in the msg class
* @in the incoming message string
* returns pointer to new Message
*/
Message::Message(string in)
{
	size_t at = 0;
	string useless = in.substr(0,at = in.find("`"));
	in = in.substr(at+1);
	from = in.substr(0,at = in.find("`"));
	in = in.substr(at+1);
	to = in.substr(0,at = in.find("`"));
	in = in.substr(at+1);
	message = in.substr(0,at = in.find("`"));
}

/*
* transcribe()
* Takes the current contents of the message fields and generates a message
* string that can be sent to the intended recipient.
* returns a message string
*/
string Message::transcribe()
{
	string out = "`" + from + "`" + to + "`" + message + "`\0";
	return out;
}

/*
* writeMessage(string msg)
* Edits the current message.
* @msg the new message contents
* returns a transcribed outgoing message
*/
string Message::writeMessage(string msg)
{
	message = msg;
	return transcribe();
}

/*
	getFrom()
	returns the message sender
*/
string Message::getFrom()
{
	return from;
}

/*
	getTo()
	returns the message recipient
*/
string Message::getTo()
{
	return to;
}

/*
	getMsg()
	returns the message
*/
string Message::getMsg()
{
	return message;
}

/*charToString(char* in)
*Converts from an array of char to a string.
*@param in: The Char array to be converted
*returns out: The string equivilent of in.
*/
string charToString(char* in)
{
	// cout<<"Entering charToString"<<endl;
	string out = "";
	int i = 0;
	while(in[i]!='\0')
	{
		out+=in[i];
		// cout<<in[i]<<endl;
		i++;
	}
	return out;
}

/*
*stringToChar(string in, char* out)
*converts a string into it's array of char equivilent
*@param in: the string being copied
*@param out: pointer to the array of char output.
*/
void stringToChar(string in, char* out)
{
	for(int i = 0; i<in.length(); i++)
	{
		out[i] = in.at(i);
	}
	out[in.length()] = '\0';
}

/*
* setUpMem(int keyNum)
* @param key, a unique memory identifier, equal in both processes to coordinate
* locations.
* @returns, Char* a pointer to the string stored in shared memory.
*/
char* setUpMem(int keyNum)
{
	key_t key = ftok("shm.shm",keyNum);
	int shmid = shmget(key,64,0640|IPC_CREAT);
	char* out = (char*) shmat(shmid,(void*)0,0);
	return out;
}

void offerHandShake(string to, string from, char* mem)
{
	stringToChar(from,mem);

	string compare = "";
	do
	{
		compare = charToString(mem);
	}while(compare.compare(to));
}

void returnHandShake(string from, string to, char* mem)
{
	string compare = "";
	do {
		compare = charToString(mem);
	} while(compare.compare(from));

	stringToChar(to,mem);
}
