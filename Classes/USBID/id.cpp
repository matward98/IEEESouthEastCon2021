#include <iostream>
#include <string>
#include <unistd.h>
#include "Serial.h"
#include "Message.h"

using namespace std;
const string WHOAMI = "RasPi";

int main()
{
  usleep(5000000);
  Serial* out = new Serial("/dev/ttyACM0");
  Message* toNano = new Message(WHOAMI,"Nano");
  out->writeStream(toNano->writeMessage("Blink"));
  usleep(70000);
  out->writeStream(toNano->writeMessage("Blink"));
  usleep(5000000);
  out->writeStream(toNano->writeMessage("Stop"));
  delete(out);
  for(int i = 0; i<2; i++)
  {
    string path = "/dev/ttyUSB"+to_string(i);
    Serial* out = new Serial(path);
    out->writeStream(toNano->writeMessage("Blink"));
    usleep(70000);
    out->writeStream(toNano->writeMessage("Blink"));
    usleep(5000000);
    out->writeStream(toNano->writeMessage("Stop"));
    delete(out);
  }
  delete(toNano);
  return 0;
}
