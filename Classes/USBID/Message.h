#ifndef MESSAGE
#define MESSAGE

#include <string>

using namespace std;

class Message
{
	string from;
	string to;
	string message;

public:
	Message(string,string,string=""); //Outgoing Message Constructor
	Message(string); //Incoming Message Constructor
	string transcribe(); //Returns a formatted Outgoing message
	string writeMessage(string); //Changes a message, leaves from and to alone
	string getFrom(); //Returns who a message is from
	string getTo(); //Returns who a message is to
	string getMsg(); //Returns the message
};

void stringToChar(string,char*);
string charToString(char*);
char* setUpMem(int);
void offerHandShake(string, string, char*);
void returnHandShake(string, string, char*);

#endif
