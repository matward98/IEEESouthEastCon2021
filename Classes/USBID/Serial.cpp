#include "Serial.h"
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <iostream>

// Information from https://blog.mbedded.ninja/programming/operating-systems/linux/linux-serial-ports-using-c-cpp/
//Termio has a Man page

/*
* Serial(string prtNme)
* opens a filehandler to the designated portName. File handler can both read and
* write, and reading is non-blocking.
* @prtNme - the /dev/ filepath to the serial port.
* returns - pointer to the Serial object.
*/
Serial::Serial(string prtNme)
{
	portName = prtNme;
	struct termios tty;
	char filePath[256] = "";
	for(int i = 0; i< portName.length(); i++)
	{
			filePath[i]=portName.at(i);
	}
	char* filePathPoint = filePath;
	fileStream = open(filePathPoint,O_RDWR|O_NONBLOCK);

	if(fileStream < 0)
	{
		cout<<"Error: "<<errno<<" "<<endl;
	}
	else
	{
		if(tcgetattr(fileStream,&tty)!=0)
		{
			cout<<"Error: "<<errno<<" "<<endl;
		}
		else
		{
			//Set Baud rate to 9600
			cfsetispeed(&tty,B9600);
			cfsetospeed(&tty,B9600);
		}

		if(tcsetattr(fileStream,TCSANOW,&tty)!=0)
		{
			cout<<"Error: "<<errno<<" "<<endl;
		}
	}
}

/*
* Serial::writeStream(string in)
* Writes string in to the serial port. Formatted for Arduinos
*/
void Serial::writeStream(string in)
{
	cout<<"Write In: "<<in<<endl;
	struct termios tty;
	if(tcgetattr(fileStream,&tty)!=0)
	{
		cout<<"Error: "<<errno<<" "<<endl;
	}
	else
	{
		/* 8 bits, no parity, no stop bits */
	 tty.c_cflag &= ~PARENB;
	 tty.c_cflag &= ~CSTOPB;
	 tty.c_cflag &= ~CSIZE;
	 tty.c_cflag |= CS8;
	 /* no hardware flow control */
	 tty.c_cflag &= ~CRTSCTS;
	 /* enable receiver, ignore status lines */
	 tty.c_cflag |= CREAD | CLOCAL;
	 /* disable input/output flow control, disable restart chars */
	 tty.c_iflag &= ~(IXON | IXOFF | IXANY);
	 /* disable canonical input, disable echo,
	 disable visually erase chars,
	 disable terminal-generated signals */
	 tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
 	 /* disable output processing */
 	 tty.c_oflag &= ~OPOST;

		if(tcsetattr(fileStream,TCSANOW,&tty)!=0)
		{
			cout<<"Error: "<<errno<<" "<<endl;
		}
	}
	char outBuff[in.length()+1];
	for(int i = 0; i < in.length(); i++)
	{
			outBuff[i] = in.at(i);
	}
	outBuff[in.length()]='\0';
	cout<<"OutBuff: "<<outBuff<<endl;
	tcflush(fileStream,TCOFLUSH);
	int writeChars = write(fileStream,&outBuff,sizeof(outBuff));
	cout<<writeChars<<" bytes written"<<endl;
}

/*
* Serial::closeStream()
* Closes the filehandler to the serial port.
*/
void Serial::closeStream()
{
	close(fileStream);
}

/*
* Serial::readString()
* Returns - the contents of the serial in buffer
*/
string Serial::readString()
{
	struct termios tty;
	if(tcgetattr(fileStream,&tty)!=0)
	{
		cout<<"Error: "<<errno<<" "<<endl;
	}
	else
	{
		tty.c_lflag |= ICANON;
		tty.c_cflag &= ~(CLOCAL | CREAD);
		tty.c_iflag |= (IXOFF | IXANY);

		if(tcsetattr(fileStream,TCSANOW,&tty)!=0)
		{
			cout<<"Error: "<<errno<<" "<<endl;
		}
	}
	char inBuff[64] = "";
	read(fileStream,&inBuff,sizeof(inBuff));
	tcflush(fileStream,TCIFLUSH);
	string out = inBuff;
	return out;
}
