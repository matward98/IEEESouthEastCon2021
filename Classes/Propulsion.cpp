#include <iostream>
#include <string>
#include <thread>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include "Serial.h"
#include "Message.h"

const string WHOAMI="RasProp";
//Propulsion Address Mac: /dev/cu.usbmodem14101
//Propulsion Address RasPi: /dev/ttyUSB0
Serial* ardNan0 = new Serial("/dev/ttyUSB0");

void propulsion(string);
void cameraFunc();

using namespace std;

int main()
{ 	/*Set up shared memory*/
	usleep(10000000);
	char* navToProp = setUpMem<char>(240);
	string currentCommand = charToString(navToProp);
	cout<<"Propulsion Memory Set Up"<<endl;
	returnHandShake("RasNav",WHOAMI,navToProp);
	cout<<"Propulsion Handshake Complete"<<endl;
	usleep(2000000);
	/*Read the initial command, probably gibberish*/
	Message* navMsg = new Message(currentCommand);
//	cout<<"navMsg: "<<navMsg->transcribe()<<endl;
//	if(navMsg->getTo()==WHOAMI)
//	{
//		propulsion(navMsg->getMsg());
//	}
//	else
//	{
//		propulsion("Brake");
//	}
	propulsion("Brake");
	/*Poll for new commands*/
	while(1)
	{
		cout<<"Cur Comm.: "<<currentCommand<<endl;
		cout<<"navToProp: "<<*navToProp<<endl;
		if(currentCommand.compare(charToString(navToProp)))
		{
			currentCommand = charToString(navToProp);
			delete(navMsg);
			navMsg = new Message(currentCommand);
			cout<<"navMsg: "<<navMsg->transcribe()<<endl;
			if(navMsg->getTo()==WHOAMI)
			{
				if(!navMsg->getMsg().compare("End"))
				{
					break;
				}
				else if(!navMsg->getMsg().compare("Camera"))
				{
					thread camera(cameraFunc);
					camera.join();
				}
				else
				{
					propulsion(navMsg->getMsg());
				}
			}
		}
		usleep(1000);
	}
	delete(navMsg);
	ardNan0->closeStream();
	cleanMem(navToProp,240);
	return 0;
}

void propulsion(string order)
{
	char output = 0x00;
	if(!order.compare("Brake"))
	{
		ardNan0->writeByte(output);
	}
	else if(order.find(":")<255)
	{
		size_t at = 0;
		string direction = order.substr(0,at = order.find(":"));
		at++;
		string speed = order.substr(at);

		if(!direction.compare("Forward"))
		{
			output &= ~(0x30);
		}
		else if(!direction.compare("Reverse"))
		{
			output |= 0x30;
		}
		else if(!direction.compare("Port"))
		{
			output &= ~(0x10);
			output |= 0x20;
		}
		else if(!direction.compare("Starboard"))
		{
			output &= ~(0x20);
			output |= 0x10;
		}

		if(!speed.compare("Full"))
		{
			output &= ~(0x02);
			output |= 0x05;
		}
		else if(!speed.compare("Half"))
		{
			output &= ~(0x03);
			output |= 0x04;
		}
		else if(!speed.compare("Quarter"))
		{
			output &= ~(0x04);
			output |= 0x03;
		}
		else if(!speed.compare("Eighth"))
		{
			output &= ~(0x05);
			output |= 0x02;
		}
		else if(!speed.compare("Sixteenth"))
		{
			output &= ~(0x06);
			output |= 0x01;
		}
	}
	ardNan0->writeByte(output);
}

void cameraFunc()
{
	char* camToProp = setUpMem<char>(450);
	returnHandShake("RasCam",WHOAMI,camToProp);
	cout<<"Camera-Propulsion Handshake Complete"<<endl;
	string currentCommand = charToString(camToProp);
	while(1)
	{
		if(currentCommand.compare(charToString(camToProp)))
		{
			currentCommand = charToString(camToProp);
			Message camMsg = Message(currentCommand);
			if(!camMsg.getTo().compare(WHOAMI))
			{
				if(!camMsg.getMsg().compare("Quit"))
				{
					break;
				}
				else
				{
					propulsion(camMsg.getMsg());
				}
			}
		}
	}
	cleanMem(camToProp,450);
}
