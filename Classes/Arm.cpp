#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/shm.h>
#include "Serial.h"
#include "Message.h"

const string WHOAMI="RasArm";

Serial* ardNan0 = new Serial("/dev/ttyUSB2");
Message* toNano = new Message(WHOAMI,"NanArm");

void propulsion(string);

using namespace std;

int main()
{
	char* camToArm = setUpMem<char>(560);
	returnHandShake("RasCam",WHOAMI,camToArm);
	cout<<"Camera-Propulsion Handshake Complete"<<endl;
	string currentCommand = charToString(camToArm);
	while(1)
	{
		if(currentCommand.compare(charToString(camToArm)))
		{
			currentCommand = charToString(camToArm);
			Message camMsg = Message(currentCommand);
			if(!camMsg.getTo().compare(WHOAMI))
			{
				if(!camMsg.getMsg().compare("Quit"))
				{
					break;
				}
				else
				{
					propulsion(camMsg.getMsg());
				}
			}
		}
	}
	cleanMem(camToArm,560);
	return 0;
}

void propulsion(string order)
{
	ardNan0->writeStream(toNano->writeMessage(order));
	cout<<"Arm writing to Nano:"<<order<<endl;
	usleep(70000);
	string in = ardNan0->readString();
	if(in.compare(""))
	{
			cout<<in<<endl;

	}
}
