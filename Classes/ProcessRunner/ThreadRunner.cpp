#include <iostream>
#include <thread>
#include <string>
#include <cmath>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/ipc.h>

using namespace std;

int main(int argc, char** argv)
{
	if(argc > 1)
	{
		string in = argv[1];
		key_t key = ftok("shm.shm",stoi(in));
		int shmid = shmget(key,1024,0640|IPC_CREAT);
		double* operand = (double*) shmat(shmid,(void*)0,0);
		double value = *operand;
		while(1)
		{
			if(value != *operand)
			{
				value = *operand;
				cout<<"Sine: "<<*operand<<endl;
			}
		}
	}
	else
	{
		cout<<"Enter an Integer when calling this program."<<endl;
	}

	return 0;
}
