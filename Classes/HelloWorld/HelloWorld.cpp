#include <wiringPi.h>
#include <unistd.h>
#include <iostream>
#include <string>

using namespace std;

#define START 0

const string OUT = "Hello World";
int main()
{
	wiringPiSetup();
	pinMode(START,INPUT);
	pullUpDnControl(START,PUD_DOWN);

	while(!digitalRead(START))
	{

	}
	while(digitalRead(START))
	{
		cout<<OUT<<endl;
		usleep(5000000);
	}
	return 0;
}
