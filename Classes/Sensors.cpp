#include <string>
#include <iostream>
#include <thread>
#include <unistd.h>
#include <sys/shm.h>
#include "Serial.h"
#include "Message.h"

using namespace std;

const string WHOAMI = "RasSense";

int sent = 1;
int isCam = 0;

void wallThread(int*, int*);
void imuThread(int*, int*);

Serial* sensorNano = new Serial("/dev/ttyUSB1");
Serial* ble = new Serial("/dev/ttyACM0");

Message* toWall = new Message(WHOAMI,"WallNano");
Message* toBle = new Message(WHOAMI, "Ble");
Message* toCam = new Message(WHOAMI, "RasCam");

char* hand = setUpMem<char>(23);
int* wallSense0 = setUpMem<int>(230);
int* wallSense1 = setUpMem<int>(231);
int* wallSense2 = setUpMem<int>(232);
int* wallSense3 = setUpMem<int>(233);
int* wallSense4 = setUpMem<int>(234);
int* wallSense5 = setUpMem<int>(235);
double* imuHeading = setUpMem<double>(236);

int main()
{
	cout<<"Sensor Memory set up."<<endl;
	usleep(10000000);

	returnHandShake("RasNav",WHOAMI,hand);
	cout<<"Sensor Handshake Complete"<<endl;

	*wallSense0 =0;
	*wallSense1 =0;
	*wallSense2 =0;
	*wallSense3 =0;
	*wallSense4 =0;
	*wallSense5 =0;

	thread wall(wallThread,&sent,&isCam);
	thread imu(imuThread,&sent,&isCam);

	while(sent)
	{
		usleep(99000);
		Message sentinal = Message(charToString(hand));
		if(!sentinal.getTo().compare(WHOAMI))
		{
			if(!sentinal.getMsg().compare("End"))
			{
				sent = 0;
			}
			else if(!sentinal.getMsg().compare("Camera"))
			{
				isCam = 1;
				returnHandShake("RasCam",WHOAMI,hand);
			}
			else if(!sentinal.getMsg().compare("Quit"))
			{
				isCam = 0;
			}
		}

		if(!sentinal.getTo().compare(WHOAMI) && !isCam)
		{
			if(!sentinal.getMsg().compare("Reset"))
			{
				ble->writeStream(toBle->writeMessage("Reset"));
				stringToChar("",hand);
			}
		}
	}

	wall.join();
	imu.join();

	sensorNano->closeStream();
	ble->closeStream();
	delete(toWall);
	delete(toBle);
	cleanMem(hand,23);
	cleanMem(wallSense0,230);
	cleanMem(wallSense1,231);
	cleanMem(wallSense2,232);
	cleanMem(wallSense3,233);
	cleanMem(wallSense4,234);
	cleanMem(wallSense5,235);
	cleanMem(imuHeading,236);
	cout<<"Sensors Terminating"<<endl;
	return 0;
}

void wallThread(int* sentinal, int* cam)
{
	while(*sentinal)
	{
		int readSense0 = -1;
		int readSense1 = -1;
		int readSense2 = -1;
		int readSense3 = -1;
		int readSense4 = -1;
		int readSense5 = -1;
		sensorNano->getWallSensors(&readSense0,&readSense1,&readSense2,&readSense3,&readSense4,&readSense5);
		if(readSense0 != -1 && readSense0 != 0)
		{
			*wallSense0 = readSense0;
			*wallSense1 = readSense1;
			*wallSense2 = readSense2;
			*wallSense3 = readSense3;
			*wallSense4 = readSense4;
			*wallSense5 = readSense5;

		}
		if(*cam && readSense2 != -1 && readSense2 != 0)
		{
			stringToChar(toCam->writeMessage(to_string(readSense2)),hand);
		}
		usleep(67000);
	}
}

void imuThread(int* sentinal, int* cam)
{
	ble->writeStream(toBle->writeMessage("IMU Mode"));
	ble->writeStream(toBle->writeMessage("IMU Mode"));
	while(*sentinal)
	{
		if(!*cam)
		{
			Message bleMsg = Message(ble->readString());
			if(!bleMsg.getTo().compare(WHOAMI))
			{
				*imuHeading = stod(bleMsg.getMsg());
			}
		}
		usleep(52000);
	}
}
