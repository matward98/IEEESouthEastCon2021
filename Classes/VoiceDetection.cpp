#include <iostream>
#include <string>
#include <sys/shm.h>
#include <unistd.h>
#include "Message.h"
#include "Serial.h"

using namespace std;

const string WHOAMI = "RasLoc";

char* pelLocs = setUpMem<char>(120);

Serial* voiceToBle = new Serial("dev/ttyACM0");

Message* bleMsg = new Message(WHOAMI,"Ble");
Message* voiceToNav = new Message(WHOAMI,"RasNav");

int main()
{
	offerHandShake(WHOAMI, "RasNav", pelLocs);
	voiceToBle->writeStream(bleMsg->writeMessage("Get Voice"));
	string location = "";
	do {
		usleep(70000);
		Message fromBle = Message(voiceToBle->readString());
		if(!fromBle.getTo().compare(WHOAMI))
		{
			if(!fromBle.getMsg().compare("Alpha")||!fromBle.getMsg().compare("Charlie"))
			{
				location = fromBle.getMsg();
			}
			else
			{
				voiceToBle->writeStream(bleMsg->writeMessage("Get Voice"));
			}
		}
	} while(!location.compare(""));
	stringToChar(voiceToNav->writeMessage(location),pelLocs);

	//clean memory
	voiceToBle->closeStream();

	delete(voiceToBle);
	delete(bleMsg);
	delete(voiceToNav);
	cleanMem(pelLocs,120);
}
