//Include Statements
#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include "Message.h"

using namespace std;

//Define Constants
const string WHOAMI = "RasNav";
#define FRONTSENSOR *wallSense1
#define RIGHTSENSOR *wallSense2
#define LEFTSENSOR *wallSense0
#define REARSENSOR *wallSense3
#define LEFT45 *wallSense4
#define RIGHT45 *wallSense5

#define FRONTCLEARANCE 45 //Wall FRONTCLEARANCE in MM
#define SIDECLEARANCE 20 
#define HALFDELAY 500000 //Time to move half a body length at ESPEED in uS

//Function Prototypes
void rightTurn(int);
void leftTurn(int);
void forwardUntilWall(int = FRONTCLEARANCE);
void pushGhost();
int forwardUntilLOpening();
int forwardUntilROpening();
//void position();
void outbound();
void inbound();
void parseLocations(string);

//Declare outgoing messages
Message* propMsg = new Message(WHOAMI,"RasProp");
Message* senseMsg = new Message(WHOAMI,"RasSense");
Message* voiceMsg = new Message(WHOAMI,"RasLoc");

//Establish Shared Memory
char* navToProp = setUpMem<char>(240);
char* senseHand = setUpMem<char>(23);
int* wallSense0 = setUpMem<int>(230);
int* wallSense1 = setUpMem<int>(231);
int* wallSense2 = setUpMem<int>(232);
int* wallSense3 = setUpMem<int>(233);
int* wallSense4 = setUpMem<int>(234);
int* wallSense5 = setUpMem<int>(235);
double* imuHeading = setUpMem<double>(236);
char* pelLocs = setUpMem<char>(120);

//Declare Global Memory
int zones[6] = {0,0,0,0,0,0};
char args[64];
char* camArgs = args;

int main(int argc, char** argv)
{
	cout<<"Nav shared memory established"<<endl;
  //Handshake with other processes
	string pZone = "";
	usleep(300000);
	string handshake = charToString(pelLocs);
	if(!handshake.compare("RasLoc"))
	{
		returnHandShake("RasLoc",WHOAMI,pelLocs);
		cout<<"Handshake with Locator complete."<<endl;
		usleep(200000);
		pZone = checkInput<char>(WHOAMI,pelLocs);
		cout<<"Pzone: "<<pZone<<endl;
		parseLocations(pZone);
		cleanMem(pelLocs,120);
	}
	else
	{
		pZone = "Whiskey:G4:G5";
		parseLocations(pZone);
	}

	offerHandShake(WHOAMI,"RasProp",navToProp);
	cout<<"Handshake with Propulsion complete."<<endl;
	offerHandShake(WHOAMI,"RasSense",senseHand);
	cout<<"Handshake with Sensors complete."<<endl;
	stringToChar(senseMsg->writeMessage("Run"),senseHand);
	cout<<"Handshaking between classes complete."<<endl;

	*wallSense0 =0;
	*wallSense1 =0;
	*wallSense2 =0;
	*wallSense3 =0;
	*wallSense4 =0;
	*wallSense5 =0;

	stringToChar(propMsg->writeMessage("Brake"),navToProp);
	usleep(500000);
	//****************************************PROGRAM IN THE TRY BLOCK**********************************************
	try
	{
		outbound();
		inbound();
	}
	catch(int e)
	{
		cout<<"Error: "<<e<<endl;

		//End Looping Processes
		stringToChar(propMsg->writeMessage("End"),navToProp);
		stringToChar(senseMsg->writeMessage("End"),senseHand);

		//Clean Up Memory
		delete(propMsg);
		delete(senseMsg);
		cleanMem(senseHand,23);
		cleanMem(wallSense0,230);
		cleanMem(wallSense1,231);
		cleanMem(wallSense2,232);
		cleanMem(wallSense3,233);
		cleanMem(imuHeading,236);
		cleanMem(navToProp,240);
		return e;
	}

	//End Looping Processes
	stringToChar(propMsg->writeMessage("End"),navToProp);
	stringToChar(senseMsg->writeMessage("End"),senseHand);

	//Clean Up Memory
	delete(propMsg);
	delete(senseMsg);
	cleanMem(senseHand,23);
	cleanMem(wallSense0,230);
	cleanMem(wallSense1,231);
	cleanMem(wallSense2,232);
	cleanMem(wallSense3,233);
	cleanMem(wallSense4,234);
	cleanMem(wallSense5,235);
	cleanMem(imuHeading,236);
	cleanMem(navToProp,240);

	return 0;
}

void parseLocations(string pLocs)
{
	system("echo \"parseLocations\n\">>Debug.txt");
	size_t at = 0;
	string powPel = pLocs.substr(0,at = pLocs.find(":"));
	at += 2;
	pLocs = pLocs.substr(at);
	int firstGhost = stoi(pLocs.substr(0,at = pLocs.find(":")));
	at += 2;
	pLocs = pLocs.substr(at);
	int twoGhost = stoi(pLocs);

	if(!powPel.compare("Alpha"))
	{
		zones[0] = 1;
	}
	zones[firstGhost] = 2;
	zones[twoGhost] = 1;
}

void pushGhost()
{
	cout << "pushGhost" << endl;
	stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
	usleep(100000); //sleep for 100 ms
	stringToChar(propMsg->writeMessage("Brake"),navToProp);
}

void rightTurn(int angle)
{
	cout<<"Right Turn:"<<angle<<endl;
	system("echo \"right turn\n\">>Debug.txt");
	//angle = int(.7912*angle-6.4311);
	angle = int(.7000*angle-6.4311); //reduced by 5/100
	if(angle<=0)
	{
		angle = 1;
	}
	while(*imuHeading == 0){} //wait until IMU is initialized
	stringToChar(senseMsg->writeMessage("Reset"),senseHand);
	usleep(52000);
	double startHeading = *imuHeading;
	cout<<startHeading<<endl;
	stringToChar(propMsg->writeMessage("Starboard:Sixteenth"),navToProp);
	while(*imuHeading>(180-angle))
	{
		cout<<*imuHeading<<endl;
		if(*imuHeading>=startHeading)
		{
			stringToChar(propMsg->writeMessage("Starboard:Sixteenth"),navToProp);
		}
		usleep(52000);
	}
	stringToChar(propMsg->writeMessage("Brake"),navToProp);
	usleep(500000);
}

void leftTurn(int angle)
{
	system("echo \"left turn\n\">>Debug.txt");
	cout<<"Left Turn:"<<angle<<endl;
	//angle = int((0.8133*angle)-5.7152);
	angle = int((0.7000*angle)-5.7152);
	if(angle<=0)
	{
		angle = 1;
	}
	while(*imuHeading == 0){} //wait until IMU is initialized
	stringToChar(senseMsg->writeMessage("Reset"),senseHand);
	usleep(52000);
	double startHeading = *imuHeading;
	cout<<startHeading<<endl;
	stringToChar(propMsg->writeMessage("Port:Sixteenth"),navToProp);
	while(*imuHeading<(180+angle))
	{
		cout<<*imuHeading<<endl;
		if(*imuHeading<=startHeading)
		{
			stringToChar(propMsg->writeMessage("Port:Sixteenth"),navToProp);
		}
		usleep(52000);
	}
	stringToChar(propMsg->writeMessage("Brake"),navToProp);
	usleep(500000);
}

void forwardUntilWall(int clear)
{
	cout<<"ForwardUntilWall"<<endl;
	system("echo \"forward until wall\n\">>Debug.txt");
	stringToChar(propMsg->writeMessage("Forward:Eighth"),navToProp);
	while(FRONTSENSOR > clear || FRONTSENSOR == 0)
	{
		usleep(67000);
		if((RIGHT45 < SIDECLEARANCE && RIGHTSENSOR < FRONTCLEARANCE)||
			(RIGHTSENSOR < SIDECLEARANCE && RIGHT45 < FRONTCLEARANCE)||
			(RIGHT45 < SIDECLEARANCE && FRONTSENSOR < FRONTCLEARANCE)||
			(FRONTSENSOR < SIDECLEARANCE && RIGHT45 < FRONTCLEARANCE))
		{
			system("echo \"mousing left\n\">>Debug.txt");
			stringToChar(propMsg->writeMessage("Port:Eighth"),
				navToProp);
			usleep(40000);
			stringToChar(propMsg->writeMessage("Forward:Eighth"),
				navToProp);
		}
		else if ((LEFT45 < SIDECLEARANCE && LEFTSENSOR < FRONTCLEARANCE)
			||(LEFTSENSOR < SIDECLEARANCE && LEFT45 < FRONTCLEARANCE)||
			(LEFT45 < SIDECLEARANCE && FRONTSENSOR < FRONTCLEARANCE)||
			(FRONTSENSOR < SIDECLEARANCE && LEFT45 < FRONTCLEARANCE))
		{
			system("echo \"mousing right\n\">>Debug.txt");
			stringToChar(propMsg->writeMessage("Starboard:Eighth"),
				navToProp);
			usleep(40000);
			stringToChar(propMsg->writeMessage("Forward:Eighth"),
				navToProp);
		}
	}
	cout<<LEFTSENSOR<<":"<<LEFT45<<":"<<FRONTSENSOR<<":"<<RIGHT45<<":"
		<<RIGHTSENSOR<<":"<<REARSENSOR<<endl;

	stringToChar(propMsg->writeMessage("Brake"),navToProp);
	usleep(500000);
}

int forwardUntilROpening()
{
	system("echo \"forward until Right opening\n\">>Debug.txt");
	stringToChar(propMsg->writeMessage("Forward:Eighth"),navToProp);
	while(RIGHTSENSOR < 250 && FRONTSENSOR > FRONTCLEARANCE)
	{
		usleep(67000);
		if((RIGHT45 < SIDECLEARANCE && RIGHTSENSOR < FRONTCLEARANCE)||
			(RIGHTSENSOR < SIDECLEARANCE && RIGHT45 < FRONTCLEARANCE)||
			(RIGHT45 < SIDECLEARANCE && FRONTSENSOR < FRONTCLEARANCE)||
			(FRONTSENSOR < SIDECLEARANCE && RIGHT45 < FRONTCLEARANCE))
		{
			system("echo \"mousing left\n\">>Debug.txt");
			stringToChar(propMsg->writeMessage("Port:Eighth"),
				navToProp);
			usleep(40000);
			stringToChar(propMsg->writeMessage("Forward:Eighth"),
				navToProp);
		}
		else if ((LEFT45 < SIDECLEARANCE && LEFTSENSOR < FRONTCLEARANCE)
			||(LEFTSENSOR < SIDECLEARANCE && LEFT45 < FRONTCLEARANCE)||
			(LEFT45 < SIDECLEARANCE && FRONTSENSOR < FRONTCLEARANCE)||
			(FRONTSENSOR < SIDECLEARANCE && LEFT45 < FRONTCLEARANCE))
		{
			system("echo \"mousing right\n\">>Debug.txt");
			stringToChar(propMsg->writeMessage("Starboard:Eighth"),
				navToProp);
			usleep(40000);
			stringToChar(propMsg->writeMessage("Forward:Eighth"),
				navToProp);
		}
	}
	stringToChar(propMsg->writeMessage("Brake"),navToProp);
	if(FRONTSENSOR > FRONTCLEARANCE && LEFT45 > FRONTCLEARANCE &&
		RIGHT45 > FRONTCLEARANCE)
	{
		stringToChar(propMsg->writeMessage("Forward:Eighth"),navToProp);
		usleep(HALFDELAY);
		stringToChar(propMsg->writeMessage("Brake"),navToProp);
	}
	if(RIGHTSENSOR < 250 && (FRONTSENSOR > FRONTCLEARANCE &&
		LEFT45 > FRONTCLEARANCE && RIGHT45 > FRONTCLEARANCE))
	{
		return forwardUntilROpening();
	}
	return 0;
}

int forwardUntilLOpening()
{
	system("echo \"forward until left opening\n\">>Debug.txt");
	stringToChar(propMsg->writeMessage("Forward:Eighth"),navToProp);
	while(LEFTSENSOR < 250 && FRONTSENSOR > FRONTCLEARANCE)
	{
		usleep(67000);
		if((RIGHT45 < SIDECLEARANCE && RIGHTSENSOR < FRONTCLEARANCE)||
			(RIGHTSENSOR < SIDECLEARANCE && RIGHT45 < FRONTCLEARANCE)||
			(RIGHT45 < SIDECLEARANCE && FRONTSENSOR < FRONTCLEARANCE)||
			(FRONTSENSOR < SIDECLEARANCE && RIGHT45 < FRONTCLEARANCE))
		{
			system("echo \"mousing left\n\">>Debug.txt");
			stringToChar(propMsg->writeMessage("Port:Eighth"),
				navToProp);
			usleep(40000);
			stringToChar(propMsg->writeMessage("Forward:Eighth"),
				navToProp);
		}
		else if ((LEFT45 < SIDECLEARANCE && LEFTSENSOR < FRONTCLEARANCE)||
			(LEFTSENSOR < SIDECLEARANCE && LEFT45 < FRONTCLEARANCE)||
			(LEFT45 < SIDECLEARANCE && FRONTSENSOR < FRONTCLEARANCE)||
			(FRONTSENSOR < SIDECLEARANCE && LEFT45 < FRONTCLEARANCE))
		{
			system("echo \"mousing right\n\">>Debug.txt");
			stringToChar(propMsg->writeMessage("Starboard:Eighth"),
				navToProp);
			usleep(40000);
			stringToChar(propMsg->writeMessage("Forward:Eighth"),
				navToProp);
		}
	}
	stringToChar(propMsg->writeMessage("Brake"),navToProp);
	if(FRONTSENSOR > FRONTCLEARANCE && LEFT45 > FRONTCLEARANCE &&
		RIGHT45 > FRONTCLEARANCE)
	{
		stringToChar(propMsg->writeMessage("Forward:Eighth"),navToProp);
		usleep(HALFDELAY);
		stringToChar(propMsg->writeMessage("Brake"),navToProp);
	}
	if(LEFTSENSOR < 250 && (FRONTSENSOR > FRONTCLEARANCE && LEFT45 >
		FRONTCLEARANCE && RIGHT45 > FRONTCLEARANCE))
	{
		return forwardUntilLOpening();
	}
	return 0;
}

void position()
{
	string pellet = "";
	if(zones[0])
	{
		system("echo \"Pellet in Alpha\n\">>Debug.txt");
		pellet = "Alpha";
		rightTurn(90);
		forwardUntilWall(20);
		leftTurn(90);
	}
	else if(!zones[0])
	{
		pellet = "Whiskey";
		system("echo \"Pellet in Whiskey\n\">>Debug.txt");
		leftTurn(90);
		forwardUntilWall(20);
		leftTurn(90);
	}
}

void outbound()
{
	if(zones[5])//Ghost in G5
	{
	system("echo \"Ghost in G5\n\">>Debug.txt");
		if(zones[3] || zones[1])//Second Ghost on Left
		{
			system("echo \"Second ghost is on the left\n\">>Debug.txt");
			rightTurn(90);
			forwardUntilWall();
			leftTurn(90);
			forwardUntilWall();
			if(zones[0])//PP in Alpha
			{
				leftTurn(90);
				forwardUntilWall();
				leftTurn(180);
				//position();
			}
			else
			{
				rightTurn(90);
				forwardUntilWall();
				rightTurn(180);
				//position();
			}
		}
		else if(zones[4] || zones[2])//Second Ghost on right
		{
			system("echo \"Second Ghost is on the left\n\">>Debug.txt");
			leftTurn(90);
			forwardUntilWall();
			rightTurn(90);
			forwardUntilWall();
			if(zones[0])//PP in Alpha
			{
				leftTurn(90);
				forwardUntilWall();
				leftTurn(180);
				//position();
			}
			else
			{
				rightTurn(90);
				forwardUntilWall();
				rightTurn(180);
				//position();
			}
		}
	}
	else
	{
		if(zones[1]&&zones[3])
		{
			system("echo \"Both Ghost on Left\n\">>Debug.txt");
			rightTurn(90);
			forwardUntilWall();
			leftTurn(90);
			forwardUntilWall();
			if(zones[0])//PP in Alpha
			{
				leftTurn(90);
				forwardUntilWall();
				leftTurn(180);
				//position();
			}
			else
			{
				rightTurn(90);
				forwardUntilWall();
				rightTurn(180);
				//position();
			}
		}
		else if(zones[2]&&zones[4])
		{
			system("echo \"Both Ghost on Right\n\">>Debug.txt");
			leftTurn(90);
			forwardUntilWall();
			rightTurn(90);
			forwardUntilWall();
			if(zones[0])//PP in Alpha
			{
				leftTurn(90);
				forwardUntilWall();
				leftTurn(180);
				//position();
			}
			else
			{
				rightTurn(90);
				forwardUntilWall();
				rightTurn(180);
				//position();
			}
		}
		else if(zones[1]&&zones[4])
		{
			system("echo \"Ghost in G1 and G4\n\">>Debug.txt");
			forwardUntilWall();
			rightTurn(90);
			forwardUntilLOpening();
			leftTurn(90);
			forwardUntilWall();
			if(zones[0])//PP in Alpha
			{
				leftTurn(90);
				forwardUntilWall();
				leftTurn(180);
				//position();
			}
			else
			{
				rightTurn(90);
				forwardUntilWall();
				rightTurn(180);
				//position();
			}
		}
		else if(zones[2]&&zones[3])
		{
			system("echo \"Ghost in G2 and G3\n\">>Debug.txt");
			forwardUntilWall();
			leftTurn(90);
			forwardUntilROpening();
			rightTurn(90);
			forwardUntilWall();
			if(zones[0])//PP in Alpha
			{
				leftTurn(90);
				forwardUntilWall();
				leftTurn(180);
				//position();
			}
			else
			{
				rightTurn(90);
				forwardUntilWall();
				leftTurn(180);
				//position();
			}
		}
	}
}

void forwardUntilRWall()
{
	stringToChar(propMsg->writeMessage("Forward:Eighth"),navToProp);
	while(RIGHTSENSOR > 250)
	{
		
	}
	stringToChar(propMsg->writeMessage("Brake"),navToProp);
}

void forwardUntilLWall()
{
	stringToChar(propMsg->writeMessage("Forward:Eighth"),navToProp);
	while(LEFTSENSOR > 250)
	{
		
	}
	stringToChar(propMsg->writeMessage("Brake"),navToProp);
}

void inbound()
{
	if(zones[0])//We are in Alpha
	{
		if(zones[2] == 2)//hit g2 first
		{
			forwardUntilROpening();
			forwardUntilRWall();
			leftTurn(90);
			forwardUntilWall();
			stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
			usleep(500000);
			stringToChar(propMsg->writeMessage("Brake"),navToProp);
			leftTurn(180);
			forwardUntilWall();
			leftTurn(90);
			forwardUntilLOpening();
			leftTurn(90);

			if(zones[3])//2nd Ghost in G3
			{
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[5])
			{
				forwardUntilRWall();
				forwardUntilROpening();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[4])
			{
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;

			}
		}
		else if(zones[1]==2)
		{
			forwardUntilWall();
			leftTurn(180);
			forwardUntilLOpening();
			rightTurn(90);
			forwardUntilWall();
			stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
			usleep(5000000);
			stringToChar(propMsg->writeMessage("Brake"),navToProp);
			leftTurn(180);
			forwardUntilWall();
			rightTurn(90);
			forwardUntilROpening();
			rightTurn(90);

			if(zones[4])
			{
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[5])
			{
				forwardUntilLWall();
				forwardUntilLOpening();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[3])
			{
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
		}
		else if(zones[4]==2)
		{
			if(!zones[2])
			{
				forwardUntilLOpening();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				if(zones[5])
				{
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[1])
				{
					leftTurn(180);
					forwardUntilWall();
					leftTurn(90);
					forwardUntilWall();
					leftTurn(180);
					forwardUntilLOpening();
					forwardUntilLWall();
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
			else
			{
				forwardUntilLOpening();
				leftTurn(90);
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				leftTurn(180);
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				rightTurn(180);
				forwardUntilROpening();
				forwardUntilRWall();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
		}
		else if(zones[5]==2)
		{
			if(!zones[2])
			{
				forwardUntilLOpening();
				leftTurn(90);
				forwardUntilLWall();
				forwardUntilLOpening();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				leftTurn(180);
				
				if(zones[4])
				{
					forwardUntilWall();
					leftTurn(180);
					forwardUntilROpening();
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[1]||zones[3])
				{
					forwardUntilWall();
					rightTurn(180);
					forwardUntilROpening();
					rightTurn(90);
					forwardUntilWall();
					leftTurn(90);
					forwardUntilROpening();
					forwardUntilRWall();
					forwardUntilROpening();
					forwardUntilRWall();
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
			else
			{
				forwardUntilLOpening();
				leftTurn(90);
				forwardUntilRWall();
				forwardUntilROpening();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				leftTurn(180);

				if(zones[3])
				{
					forwardUntilWall();
					leftTurn(180);
					forwardUntilLOpening();
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[2]||zones[4])
				{
					forwardUntilLWall();
					forwardUntilLOpening();
					rightTurn(90);
					forwardUntilWall();
					rightTurn(90);
					forwardUntilLOpening();
					forwardUntilLWall();
					forwardUntilLOpening();
					forwardUntilLWall();
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
		}
		else if(zones[3]==2)
		{
			if(!zones[1])
			{
				forwardUntilWall();
				leftTurn(180);
				forwardUntilROpening();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				leftTurn(180);
				
				if(zones[5])
				{
					forwardUntilWall();
					leftTurn(180);
					forwardUntilRWall();
					forwardUntilROpening();
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[2])
				{
					forwardUntilWall();
					rightTurn(90);
					forwardUntilLOpening();
					forwardUntilLWall();
					forwardUntilLOpening();
					forwardUntilLWall();
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
		}
	}
	else//We are in Whiskey
	{
		if(zones[1] == 2)//hit g2 first
		{
			forwardUntilLOpening();
			forwardUntilLWall();
			rightTurn(90);
			forwardUntilWall();
			stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
			usleep(500000);
			stringToChar(propMsg->writeMessage("Brake"),navToProp);
			rightTurn(180);
			forwardUntilWall();
			rightTurn(90);
			forwardUntilROpening();
			rightTurn(90);

			if(zones[4])//2nd Ghost in G3
			{
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[5])
			{
				forwardUntilLWall();
				forwardUntilLOpening();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[3])
			{
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;

			}
		}
		else if(zones[2]==2)
		{
			forwardUntilWall();
			rightTurn(180);
			forwardUntilROpening();
			leftTurn(90);
			forwardUntilWall();
			stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
			usleep(5000000);
			stringToChar(propMsg->writeMessage("Brake"),navToProp);
			rightTurn(180);
			forwardUntilWall();
			leftTurn(90);
			forwardUntilLOpening();
			leftTurn(90);

			if(zones[3])
			{
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[5])
			{
				forwardUntilRWall();
				forwardUntilROpening();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
			else if(zones[4])
			{
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
		}
		else if(zones[3]==2)
		{
			if(!zones[1])
			{
				forwardUntilROpening();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				if(zones[5])
				{
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[2])
				{
					rightTurn(180);
					forwardUntilWall();
					rightTurn(90);
					forwardUntilWall();
					rightTurn(180);
					forwardUntilROpening();
					forwardUntilRWall();
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
			else
			{
				forwardUntilROpening();
				rightTurn(90);
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				rightTurn(180);
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				leftTurn(90);
				forwardUntilWall();
				leftTurn(180);
				forwardUntilLOpening();
				forwardUntilLWall();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				return;
			}
		}
		else if(zones[5]==2)
		{
			if(!zones[1])
			{
				forwardUntilROpening();
				rightTurn(90);
				forwardUntilRWall();
				forwardUntilROpening();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				rightTurn(180);
				
				if(zones[3])
				{
					forwardUntilWall();
					rightTurn(180);
					forwardUntilLOpening();
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[2]||zones[4])
				{
					forwardUntilWall();
					leftTurn(180);
					forwardUntilLOpening();
					leftTurn(90);
					forwardUntilWall();
					rightTurn(90);
					forwardUntilLOpening();
					forwardUntilLWall();
					forwardUntilLOpening();
					forwardUntilLWall();
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
			else
			{
				forwardUntilROpening();
				rightTurn(90);
				forwardUntilLWall();
				forwardUntilLOpening();
				rightTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				rightTurn(180);

				if(zones[4])
				{
					forwardUntilWall();
					rightTurn(180);
					forwardUntilROpening();
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[1]||zones[3])
				{
					forwardUntilRWall();
					forwardUntilROpening();
					leftTurn(90);
					forwardUntilWall();
					leftTurn(90);
					forwardUntilROpening();
					forwardUntilRWall();
					forwardUntilROpening();
					forwardUntilRWall();
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
		}
		else if(zones[4]==2)
		{
			if(!zones[2])
			{
				forwardUntilWall();
				rightTurn(180);
				forwardUntilLOpening();
				leftTurn(90);
				forwardUntilWall();
				stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				stringToChar(propMsg->writeMessage("Reverse:Quarter"),navToProp);
				usleep(500000);
				stringToChar(propMsg->writeMessage("Brake"),navToProp);
				rightTurn(180);
				
				if(zones[5])
				{
					forwardUntilWall();
					rightTurn(180);
					forwardUntilLWall();
					forwardUntilLOpening();
					rightTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
				else if(zones[1])
				{
					forwardUntilWall();
					leftTurn(90);
					forwardUntilROpening();
					forwardUntilRWall();
					forwardUntilROpening();
					forwardUntilRWall();
					leftTurn(90);
					forwardUntilWall();
					stringToChar(propMsg->writeMessage("Forward:Quarter"),navToProp);
					usleep(500000);
					stringToChar(propMsg->writeMessage("Brake"),navToProp);
					return;
				}
			}
		}
	}
}
