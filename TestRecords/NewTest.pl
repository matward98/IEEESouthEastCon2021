#!/usr/bin/perl
use warnings;
use strict;

#File Handlers
my $countRead;
my $countWrite;
my $recordWrite;

#Open File Handler to read count
open($countRead,'<',"Count.txt");
chomp(my $count =<$countRead>);
print "Making Test ${count}\n";
close($countRead);

#OpenNew File
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
$year += 1900;
$mon += 1;
my $testString = "Test${count}_${year}-${mon}-${mday}-${hour}-${min}-${sec}.txt";
`touch $testString`;
`chmod 640 $testString`;

#Write Template
open($recordWrite,'>',$testString);
print($recordWrite "Test Record #".$count."\n");
print($recordWrite "Tested at: ".$hour.":".$min.":".$sec." on ".$year."-".$mon."-".$mday."\n");
print($recordWrite "Tested by Matthew Ward\n");
print($recordWrite "Details:\n\n");
print($recordWrite "Results:\n\n");
print($recordWrite "Debugging:\n\n");
print($recordWrite "Results after Debugging:\n\n");

#Close new Test Record
close($recordWrite);

#Increment Count
open($countWrite,'>',"Count.txt");
$count++;
print($countWrite "${count}\n");
close($countWrite);
