set number
if has('filetype')
	filetype indent plugin on
endif
if has('syntax')
	syntax on
endif
set autoindent
set shiftwidth=4
set tabstop=4
colorscheme delek
